//
//  System.swift
//  Combine_MVVM
//
//  Created by Ananth Chepuri on 22/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation
import Combine

extension Publishers {
    
    static func system<State, Event, Scheduler: Combine.Scheduler> (
        initial: State,
        reduce: @escaping (State, Event) -> State,
        scheduler: Scheduler,
        feedbacks: [Feedback<State, Event>]
    ) -> AnyPublisher<State, Never> {
        
        let state = CurrentValueSubject<State, Never>(initial)
    
        let events = feedbacks.map { feedBack in feedBack.run(state.eraseToAnyPublisher()) }

        return Deferred {
            Publishers.MergeMany(events)
                .receive(on: scheduler)
                .scan(initial, reduce)
                .handleEvents(receiveOutput: state.send)
                .receive(on: scheduler)
                .prepend(initial)
                .eraseToAnyPublisher()
        }
        .eraseToAnyPublisher()
    }
}
