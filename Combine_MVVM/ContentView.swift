//
//  ContentView.swift
//  Combine_MVVM
//
//  Created by Ananth Chepuri on 22/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
