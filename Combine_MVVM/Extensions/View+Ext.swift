//
//  View+Ext.swift
//  Combine_MVVM
//
//  Created by Ananth Chepuri on 23/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView { AnyView(self) }
}

